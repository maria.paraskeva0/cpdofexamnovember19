package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class StringsTest {

    @Test
    public void test_Usefulness() {
      assertTrue("test Usefulness", new Usefulness().desc().contains("marry 2 distinct worlds"));
    }

    @Test
    public void test_Duration() {
      assertTrue("test Duration", new Duration().dur().contains("then you can opt for either half days"));
    }

    @Test
    public void test_AboutCPDOF() {
      assertTrue("test AboutCPDOF", new AboutCPDOF().desc().contains("tools in entire lifecycle"));
    }
}
