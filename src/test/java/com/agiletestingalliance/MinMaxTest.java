package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest {

  @Test
  public void test_compareInt() {
     assertEquals("A bigger than B", 3, new MinMax().compareInt(3, 1));
  }

  @Test
  public void test_compareInt2() {
     assertEquals("B bigger than A", 2, new MinMax().compareInt(1, 2));
  }

}
